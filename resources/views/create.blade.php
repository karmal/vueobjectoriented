<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css">
        <style>body { padding-top: 40px; } </style>
     
    </head>
    <body>

            <div id="app" class="container">
                {{-- include list blade --}}
                @include('list')


                {{-- submit.prevent to prevent a default action  and call onSubmit action--}}
                 {{-- event refers to original dom element that was fired , than just use target --}}
                <form method="POST" action="/projects" @submit.prevent="onSubmit" @keydown="errors.clear($event.target.name)">
                    <div class="control">
                        <label for="name" class="label">Project Name:</label>

                        <input type="text" id="name" name="name" class="input" v-model="name">
                        {{-- display errors of 'name' , only if there are errors--}}
                        <span class="help is-danger" v-if="errors.has('name')" v-text="errors.get('name')"></span>
                    </div>


                    <div class="control">
                        <label for="description" class="label">Project Description:</label>

                        <input type="text" id="description" name="description" class="input" v-model="description">

                        {{-- display errors of 'description' , and only if has errors--}}
                        <span class="help is-danger" v-if="errors.has('description')" v-text="errors.get('description')"></span>
                    </div>
                    

                    <div class="control">
                        {{-- Disable button after errors show up --}}
                        <button class="button is-primary" :disabled="errors.any()">Create</button>
                    </div>
                </form>
            </div>

            <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
            <script src="/js/app.js"></script>

    </body>
</html>



{{-- 
    1. Send data through axios
    2. Field Validation
    3. Display errors
    4. On keydown validation errors dissapear
    5. Display data from database
    --}}
