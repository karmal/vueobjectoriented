
class Errors {
    constructor() {
        // if no errors this will return nothing,
        // but if we have some errors, than we record a message
        this.errors = {};
    }

    has(field) {
        // if this.errors contains a "field" property
        // hasOwnProperty returns boolean indicating whether the object has specified property as its own
        return this.errors.hasOwnProperty(field);
    }

    any() {
        // keys, name: 'John', key is name
        // we check the length of errors object, if more than 0 means we have errors and disable the button
        return Object.keys(this.errors).length > 0;
    }

    //user try to get an error
    get(field) {
    // so for example if You have a name field or any other field
        if(this.errors[field]){
            // return first item
           return this.errors[field][0];
        }
    }

    record(errors) {
        // update our errors
        this.errors = errors;
    }

    
    clear(field) {
        delete this.errors[field];
    }
}



new Vue({
    el: '#app',


    data: {

        name: '',

        description: '',

        errors: new Errors()
    },



    methods: {

        onSubmit() {
            // post projects
            axios.post('projects', this.$data)


            // we can do it like that instead: 
            // axios.post('/projects',{
             
            //     name: this.name,

            //     description: this.description 

            // }) 



            // if success call onSucess method
            .then(this.onSuccess)
            // get errors and record them in our errors object
            .catch(error => this.errors.record(error.response.data.errors));
        },

        onSuccess(response) {
            // alert response message and clear inputs
            alert(response.data.message);
            this.name = '',
            this.description = ''
        }
        
    }
});